from rule_engine import EvaluationError
from pynomics import rules as r
from pathlib import Path
import datetime
import pytest

support_dir = Path("tests/support/")
sample_dir = support_dir / Path("sample_dir")
sample_file_names = ["sample_file.csv", "sample_file.xlsx"]
sample_files = [support_dir / Path(f) for f in sample_file_names]
sample_rules = support_dir / Path("rules.yaml")


@pytest.mark.parametrize("test_file", sample_files)
def test_can_load_import_rules(test_file):
    rules = r.Rules()
    rules.load(sample_rules)

    assert rules.import_rules is not None
    assert len(rules.import_rules) > 0
    assert all([isinstance(rule, r.ImportRule) for rule in rules.import_rules])
    assert any([r.matches_file(test_file) for r in rules.import_rules])


def test_import_rule_checks_for_file_match():
    rule = r.ImportRule(match="Transactions*.csv")

    assert rule.matches_file(Path("Transactions.csv"))
    assert rule.matches_file(Path("Subdirectory/Transactions.csv"))
    assert not rule.matches_file(Path("Subdirectory/1234 Transactions.csv"))
    assert not rule.matches_file(Path("Subdirectory/Transactions.pdf"))


@pytest.mark.parametrize("test_file", sample_files)
def test_import_rule_can_load_file(test_file):
    rules = r.Rules()
    rules.load(sample_rules)

    rule = None
    for rl in rules.import_rules:
        if rl.matches_file(test_file):
            rule = rl
            break

    assert rule is not None, "Sample file is not matched by test rule"

    data = rule.load(test_file)

    assert data is not None
    assert len(data) > 0

    # Data integrity
    for row in data:
        isinstance(row[r.ACCOUNT], str)
        isinstance(row[r.AMOUNT], float)
        isinstance(row[r.CURRENCY], str)
        isinstance(row[r.TRANSACTION_DATE], datetime.date)
        isinstance(row[r.PAYEE], str)
        isinstance(row[r.NOTE], str)


def test_rules_can_load_directory():
    rules = r.Rules()
    rules.load(sample_rules)

    data = rules.load_dir(sample_dir)

    assert isinstance(data, list)

    for row in data:
        assert isinstance(row, dict)

    # Doesn't test all rows, but a sample
    expected = [
        {
            r.ACCOUNT: "My account",
            r.AMOUNT: -500.0,
            r.CURRENCY: "SEK",
            r.TRANSACTION_DATE: datetime.date(2024, 1, 2),
            r.PAYEE: "REF A",
            r.NOTE: "Tickets",
        },
        {
            r.ACCOUNT: "A second account",
            r.AMOUNT: -250.0,
            r.CURRENCY: "SEK",
            r.TRANSACTION_DATE: datetime.date(2024, 1, 11),
            r.PAYEE: "REF D",
            r.NOTE: "Gas",
        },
    ]

    for e in expected:
        assert e in data


def test_multiple_matchers_do_not_duplicate_data():
    rules = r.Rules()
    rules.load(support_dir / Path("rules_with_duplicate_matchers.yaml"))

    data = rules.load_dir(sample_dir)

    count = sum([row[r.TRANSACTION_DATE] == datetime.date(2024, 1, 2) for row in data])

    assert count == 1


def test_non_matching_file_is_not_parsed():
    rules = r.Rules()
    rules.load(sample_rules)

    data = rules.load_dir(sample_dir)

    # Don't check an entire row, as this is prone to false positives in case
    # there are minor mismatches (for example due to typos)
    assert "Excluded account" not in [row[r.ACCOUNT] for row in data]


def test_rule_classifies_transaction_based_on_expression():
    rule = r.ClassificationRule(
        rule='payee == "REF D"',
        classifications={r.CATEGORY: "housing", r.SUBCATEGORY: "heating"},
    )

    transaction = {
        r.ACCOUNT: "A second account",
        r.AMOUNT: -250.0,
        r.CURRENCY: "SEK",
        r.TRANSACTION_DATE: datetime.date(2024, 1, 11),
        r.PAYEE: "REF D",
        r.NOTE: "Gas",
    }
    classified = rule.classify(transaction=transaction)

    assert r.CATEGORY in classified
    assert classified[r.CATEGORY] == "housing"
    assert r.SUBCATEGORY in classified
    assert classified[r.SUBCATEGORY] == "heating"


def test_rule_do_not_overwrite_previous_classification():
    rule = r.ClassificationRule(
        rule='payee == "REF D"',
        classifications={r.CATEGORY: "housing", r.SUBCATEGORY: "heating"},
    )

    transaction = {
        r.ACCOUNT: "A second account",
        r.AMOUNT: -250.0,
        r.CURRENCY: "SEK",
        r.TRANSACTION_DATE: datetime.date(2024, 1, 11),
        r.PAYEE: "REF D",
        r.NOTE: "Gas",
        r.CATEGORY: "Original category",
        r.SUBCATEGORY: "Original subcategory",
    }
    classified = rule.classify(transaction=transaction)

    assert classified[r.CATEGORY] != "housing"
    assert classified[r.SUBCATEGORY] != "heating"


def test_rule_categorizes_when_classification_is_empty():
    rule = r.ClassificationRule(
        rule='payee == "REF D"',
        classifications={r.CATEGORY: "housing", r.SUBCATEGORY: "heating"},
    )

    transaction = {
        r.ACCOUNT: "A second account",
        r.AMOUNT: -250.0,
        r.CURRENCY: "SEK",
        r.TRANSACTION_DATE: datetime.date(2024, 1, 11),
        r.PAYEE: "REF D",
        r.NOTE: "Gas",
        r.CATEGORY: "",
        r.SUBCATEGORY: "",
    }
    classified = rule.classify(transaction=transaction)

    assert classified[r.CATEGORY] == "housing"
    assert classified[r.SUBCATEGORY] == "heating"


def test_rules_classifies_transactions_in_directory():
    rules = r.Rules()
    rules.load(sample_rules)

    data = rules.load_dir(sample_dir)

    expected_classified = [row for row in data if row[r.PAYEE] == "REF C"]

    assert len(expected_classified) == 1
    expected_classified = expected_classified[0]

    assert r.CATEGORY in expected_classified
    assert expected_classified[r.CATEGORY] == "housing"
    assert expected_classified[r.SUBCATEGORY] == "heating"


def test_transactions_are_not_duplicated_when_multiple_classifiers_matches():
    rules = r.Rules()
    rules.load(support_dir / Path("rules_with_duplicate_matchers.yaml"))

    data = rules.load_dir(sample_dir)

    count = sum([row[r.TRANSACTION_DATE] == datetime.date(2024, 1, 2) for row in data])

    assert count == 1


def test_parsing_returns_useful_error_message_when_rules_file_misses_field():
    filename = "rules_with_missing_import.yaml"
    rules = r.Rules()
    try:
        rules.load(support_dir / Path(filename))
        pytest.fail("Rules should raise error on invalid rules file")
    except ValueError as e:
        assert "missing field 'import'" in str(e).lower()


def test_parsing_does_not_crash_when_type_mismatches_rule():
    rules = r.Rules()
    rules.load(support_dir / Path("rules_with_type_mismatch.yaml"))

    try:
        rules.load_dir(sample_dir)
    except EvaluationError:
        pytest.fail("Rules should handle type mismatches")


def test_parsing_raises_good_error_when_file_is_not_supported():
    rules = r.Rules()
    rules.load(support_dir / Path("rules_with_unsupported_file_format.yaml"))
    try:
        rules.load_dir(sample_dir)
        pytest.fail("Rules should raise error on unsupported file format")
    except ValueError as e:
        assert "unsupported file format" in str(e).lower()
