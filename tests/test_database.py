import random
import string
from datetime import date
from pathlib import Path
from pynomics.database import Database, Transaction
from pynomics import rules


def random_string(length):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(length))


def transaction(
    transaction_date: date | None = None,
    amount: int | None = None,
    category: str | None = None,
) -> Transaction:
    # Amount is rounded or we get issues when comparing amounts, as float
    # values may differ in very insignificant decimal places
    if transaction_date is None:
        transaction_date = date(2024, 1, 1)
    if amount is None:
        amount = round(random.random() * 400)

    t1 = {
        rules.ACCOUNT: random_string(5),
        rules.CATEGORY: category,
        rules.TRANSACTION_DATE: transaction_date,
        rules.AMOUNT: amount,
        rules.NOTE: random_string(10),
    }
    return t1


def test_database_can_merge_with_other_database():
    t1 = transaction()
    t2 = transaction()
    db1 = Database([t1])
    db2 = Database([t2])

    count = db1.merge(db2)

    assert 1 == count
    assert t2 in db1.transactions


def test_database_do_not_merge_identical_transactions():
    duplicate = transaction()
    unique = transaction()
    db1 = Database([duplicate, unique])
    db2 = Database([duplicate])

    count = db1.merge(db2)

    assert 0 == count
    assert 1 == len([t for t in db1.transactions if t == duplicate])


def test_database_can_save_and_load_from_file():
    filename = __name__ + ".xlsx"
    db1 = Database([transaction(), transaction()])
    db1.save(Path(filename))

    db2 = Database([])
    db2.load(Path(filename))

    assert db1.transactions == db2.transactions


def test_empty_database_can_save_and_load():
    filename = __name__ + ".xlsx"
    db1 = Database([])
    db1.save(Path(filename))

    db2 = Database([])
    db2.load(Path(filename))

    assert db1.transactions == db2.transactions


def test_database_returns_sum_per_category():
    amount1 = 30
    amount2 = 40
    category1 = "Category A"
    category2 = "Category B"
    n_1 = 500
    n_2 = 600

    sum1 = n_1 * amount1
    sum2 = n_2 * amount2

    print([transaction()] + [transaction()])

    db = Database(
        [transaction(amount=amount1, category=category1) for _ in range(n_1)]
        + [transaction(amount=amount2, category=category2) for _ in range(n_2)]
    )

    assert db.aggregate_for_category(sum, category1) == sum1
    assert db.aggregate_for_category(sum, category2) == sum2


def test_database_can_return_categories():
    db = Database([
        transaction(category="A"),
        transaction(category="B"),
        transaction(category="A"),
    ])

    assert db.categories == {"A", "B"}


def test_empty_database_returns_empty_set_of_categories():
    db = Database()
    assert db.categories == set()


def test_database_returns_earliest_start_date():
    db = Database([
        transaction(transaction_date=date(year=2013, month=3, day=5)),
        transaction(transaction_date=date(year=2011, month=10, day=6)),
        transaction(transaction_date=date(year=2013, month=3, day=5)),
    ])

    assert db.start_date == date(year=2011, month=10, day=6)


def test_database_returns_latest_end_date():
    db = Database([
        transaction(transaction_date=date(year=2011, month=10, day=6)),
        transaction(transaction_date=date(year=2013, month=3, day=5)),
        transaction(transaction_date=date(year=2011, month=10, day=6)),
    ])

    assert db.end_date == date(year=2013, month=3, day=5)


def test_database_returns_duration():
    db = Database([
        transaction(transaction_date=date(year=2011, month=10, day=6)),
        transaction(transaction_date=date(year=2013, month=3, day=5)),
        transaction(transaction_date=date(year=2011, month=10, day=6)),
    ])

    assert db.duration == 517


def test_database_returns_zero_days_for_duration_of_zero_transactions():
    db = Database()

    assert db.duration == 0
