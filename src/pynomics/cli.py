import os
import sys
import time
from typing import Callable

import click
import locale
from rich.console import Console
from rich.table import Table
from rich.theme import Theme

from pynomics.database import Database
from pynomics.rules import Rules
from pynomics.stats import Stats, TableType

theme = Theme({"error": "red bold", "warning": "yellow"})
console = Console(theme=theme)


def _handle_exception(user_message: str, exception: Exception):
    console.print(user_message, style="error")
    console.print(exception)
    sys.exit(-1)


def _run(call: Callable, error_message: str, *args, **kwargs):
    res = None
    try:
        res = call(*args, **kwargs)
    except Exception as e:
        _handle_exception(error_message, e)

    return res


@click.group()
@click.option(
    "-db",
    "--database",
    type=click.Path(exists=False),
    default="master.xlsx",
    help="Database file to use, if not default",
)
@click.pass_context
def cli(ctx: click.Context, database: os.PathLike):
    """Pynomics is a program designed to help in automatically classify
    transactions based on a set of rules, and then provide insights into
    your finances.

    Use one of the two subcommands; `load` and `insights`."""
    master = Database([])
    if not os.path.exists(database):
        console.print("Master database does not exist. Creating it.")
        _run(master.save, "Failed to create master database.", database)
    else:
        _run(master.load, "Failed to load master database.", database)

    ctx.ensure_object(dict)
    ctx.obj["DATABASE"] = master
    ctx.obj["DATABASE_PATH"] = database


@cli.command()
@click.argument("dir")
@click.option(
    "-r",
    "--rules_file",
    type=click.Path(exists=True),
    default="rules.yaml",
    help="Rules file to use",
)
@click.pass_context
def load(ctx: click.Context, dir, rules_file: str):
    """Parse and classify data into master file"""

    if rules_file is None:
        default_rules = "rules.yaml"
        if os.path.exists(default_rules):
            console.print(
                f"No rules file was specified. Defaulting to '{default_rules}'."
            )
            rules_file = default_rules
        else:
            console.print(
                "No rules file was specified and no default file was found.",
                style="error",
            )
            sys.exit(-1)
    console.print(f"Processing files in '{dir}' using rules file '{rules_file}'.")

    rules = Rules()
    _run(rules.load, "Failed to load rules file", rules_file)

    data = None
    process_time = 0
    console.print("Processing files.")
    try:
        start = time.time()
        data = rules.load_dir(dir)
        end = time.time()
        process_time = end - start
    except Exception as e:
        _handle_exception("Failed to load data files", e)

    console.print(
        f" - {len(data)} transactions processed in {process_time:.2f} seconds."
    )

    db = Database(data)

    master = ctx.obj["DATABASE"]
    output = ctx.obj["DATABASE_PATH"]
    console.print("Merging parsed data to master database.")
    n_new = _run(master.merge, "Failed to merge databases.", db)
    n_discarded = len(db.transactions) - n_new
    console.print(f" - {n_new} new transactions were added to database.")
    console.print(
        f" - {n_discarded} transactions were considered duplicated and discarded."
    )
    console.print("Saving merged records to database.")
    _run(master.save, "Failed to save database.", output)

    console.print(f"Database was written to {output}")


@cli.command()
@click.option("--summary-table", is_flag=True, default=False)
@click.pass_context
def stats(ctx: click.Context, summary_table: bool):
    """Print statistics of the transactions."""
    if not summary_table:
        console.print(
            "No statistics selected. Use 'pynomics stats --help' for more info.",
            style="error",
        )
        sys.exit(-1)

    master = ctx.obj["DATABASE"]
    stats = Stats(master)

    tables = {TableType.SUMMARY_TABLE: summary_table}

    for table_type, make in tables.items():
        if make:
            table = stats.make_table(table_type)
            print_table = Table(title=str(table_type))
            for col in table["columns"]:
                justification = "left" if col["type"] is str else "right"
                print_table.add_column(col["title"], justify=justification)
            for row in table["rows"]:

                def format_cell(cell) -> str:
                    if isinstance(cell, float):
                        color = "red" if cell < 0 else "green"
                        return f"[{color}]{locale.format_string('%.0f', cell, True)}[/{color}]"
                    return str(cell)

                print_table.add_row(*[format_cell(cell) for cell in row])

            console.print(print_table)


@cli.command()
def insights():
    """Generate tables and graphs to provide insights into your finances"""
    raise NotImplementedError("Insights are not yet implemented.")
