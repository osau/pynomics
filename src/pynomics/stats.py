from pynomics.database import Database, CATEGORY, AMOUNT
from enum import StrEnum

Table = dict[str, list]


class TableType(StrEnum):
    SUMMARY_TABLE = "Summary table"


class Stats:
    def __init__(self, db: Database) -> None:
        self._db = db

    def _make_summary_table(self) -> Table:
        categories = {c: 0 for c in self._db.categories}
        for transaction in self._db.transactions:
            category = transaction[CATEGORY]
            amount = transaction[AMOUNT]
            categories[category] += amount

        years = self._db.duration / 365.0
        sorted_keys = sorted(categories)
        return {
            "columns": [
                {"title": "Category", "type": str},
                {"title": "Yearly avg.", "type": int},
            ],
            "rows": [(c, categories[c] / years) for c in sorted_keys],
        }

    def make_table(self, type: TableType) -> Table:
        if type == TableType.SUMMARY_TABLE:
            return self._make_summary_table()

        raise ValueError(f"Unknown table type {type}")
