"""Module for database abstraction

The database abstraction's purpose is to hide the details on how processed
data is being stored and manipulated."""

import os
from datetime import date
from typing import List, Dict, Callable
import pandas as pd

ACCOUNT = "account"
AMOUNT = "amount"
CURRENCY = "currency"
TRANSACTION_DATE = "transaction_date"
PAYEE = "payee"
NOTE = "note"
CATEGORY = "category"
SUBCATEGORY = "subcategory"

Transaction = Dict


class Database:
    """Database abstraction

    The database contains functionality to load, store and merge data that has
    been parsed from the other modules.
    """

    def __init__(self, data: List[Transaction] = []) -> None:
        self._data = data

    def _filter(
        self, transactions: list[Transaction] | None = None, category: str | None = None
    ) -> list[Transaction]:
        if transactions is None:
            transactions = self.transactions
        filtered = []
        for t in self.transactions:
            if t[CATEGORY] == category:
                filtered.append(t)

        return filtered

    def _aggregate(
        self,
        transactions: list[Transaction] | None = None,
        column: str = AMOUNT,
        func: Callable[[list[float | int]], float | int] = sum,
    ) -> float | int:
        if transactions is None:
            transactions = self.transactions
        return func([t[column] for t in transactions])

    @staticmethod
    def is_duplicate(t1: Transaction, t2: Transaction) -> bool:
        keys = [ACCOUNT, AMOUNT, TRANSACTION_DATE]

        for k in keys:
            if k in t1 and k in t2 and t1[k] != t2[k]:
                return False

        return True

    def save(self, path: os.PathLike) -> None:
        # Only use of pandas; to convert to excel file.
        # Note that could have been accomplished using other libraries, but
        # since pandas is used to generate insights, it can as well be used
        # here.
        df = pd.DataFrame(self._data)
        df.to_excel(path)

    def load(self, path: os.PathLike) -> None:
        df = pd.read_excel(path)

        # Remove bogus columns
        to_filter = ["Unnamed: 0"]
        for f in to_filter:
            if f in df:
                del df[f]

        # orient=records gives a list of dictonaries, instead of dictionary of
        # lists
        self._data = df.to_dict(orient="records")

        # Convert date to correct type
        for transaction in self._data:
            ts = transaction[TRANSACTION_DATE]
            if pd.isna(transaction[CATEGORY]):
                transaction[CATEGORY] = None
            transaction[TRANSACTION_DATE] = date(ts.year, ts.month, ts.day)

    def merge(self, other: "Database") -> int:
        """Merges a database into the current

        This happens in-place, meaning that `self` will be updated with
        values from `other`

        Args:
            other (Database): Other database to be merged with

        Returns:
            int: Number of merged transactions
        """
        # Store merged data in separate variable to avoid modifying collections
        # during iteration
        merged = self._data.copy()
        n_orig = len(merged)
        for other_t in other.transactions:
            match = True
            for self_t in self.transactions:
                if Database.is_duplicate(self_t, other_t):
                    match = False
                    break
            if match:
                merged.append(other_t)

        n_final = len(merged)

        self._data = merged

        assert n_final >= n_orig, "Number of records in database shrunk during merge"

        return n_final - n_orig

    @property
    def transactions(self) -> List[Transaction]:
        return self._data

    @property
    def categories(self) -> set[str]:
        categories = set()
        for t in self.transactions:
            categories.add(t[CATEGORY])
        return categories

    @property
    def start_date(self) -> date:
        return self._aggregate(column=TRANSACTION_DATE, func=min)

    @property
    def end_date(self) -> date:
        return self._aggregate(column=TRANSACTION_DATE, func=max)

    @property
    def duration(self) -> int:
        if len(self.transactions) == 0:
            return 0
        return (self.end_date - self.start_date).days + 1

    def aggregate_for_category(
        self, func: Callable[[list[float | int]], float | int], category: str
    ) -> float | int:
        transactions = self._filter(category=category)
        return self._aggregate(transactions=transactions, func=func)
