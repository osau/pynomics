"""Rules for parsing

This module defines interfaces for rules
"""

import os
import re
import datetime
from typing import List, Dict
from pathlib import Path
from ruamel.yaml import YAML
from rule_engine import EvaluationError, Rule
import pandas as pd

from pynomics.database import (
    ACCOUNT,
    AMOUNT,
    CATEGORY,
    CURRENCY,
    TRANSACTION_DATE,
    PAYEE,
    SUBCATEGORY,
    NOTE,
)

ALL_DEFAULT_COLUMNS = [
    ACCOUNT,
    AMOUNT,
    CURRENCY,
    TRANSACTION_DATE,
    PAYEE,
    NOTE,
]

ALL_COLUMNS = ALL_DEFAULT_COLUMNS + [CATEGORY, SUBCATEGORY]

default_columns = {c: i for i, c in enumerate(ALL_DEFAULT_COLUMNS)}


class ImportRule:
    """Rules for importing data

    Import rules have the following responsibilities:

     - Decide if a given file/path matches the critiera
     - Load the file into a given format
    """

    def __init__(
        self,
        match: str,
        account: str = "",
        skip_rows: int = 0,
        columns: Dict = default_columns,
        date_format: str = str(datetime.datetime.isoformat),
    ) -> None:
        self._regex = re.compile(match)
        self._skip_rows = skip_rows
        self._columns = {v: k for k, v in columns.items()}
        self._date_format = date_format

    def matches_file(self, file: os.PathLike) -> bool:
        """If a given file matches the rule

        Note that the path is not considered, only the file name. It is
        therefore safe to test relative and absolute paths.

        Args:
            file (os.PathLike): File to test

        Returns:
            bool: True if matches, otherwise false
        """

        basename = os.path.basename(file)
        return self._regex.match(basename) is not None

    def load(self, path: os.PathLike) -> List[Dict]:
        filename = os.path.basename(path)
        extension = filename.split(".")[-1] if "." in filename else filename

        loader = None
        if extension == "csv":
            loader = pd.read_csv
        elif extension == "xlsx" or extension == "xls":
            loader = pd.read_excel
        else:
            raise ValueError(f"Unsupported file format {extension}")
        df = loader(path, skiprows=self._skip_rows)

        # Rename columns according to rules
        df = df.rename(columns=self._columns)

        # Drop unsupported columns
        unsupported = [c for c in df.columns if c not in ALL_COLUMNS]
        df = df.drop(columns=unsupported)
        res = df.to_dict(orient="records")

        # Type conversions
        for row in res:
            td = row[TRANSACTION_DATE]
            if isinstance(td, str):
                row[TRANSACTION_DATE] = datetime.datetime.strptime(
                    td, self._date_format
                ).date()
            if isinstance(td, pd.Timestamp):
                row[TRANSACTION_DATE] = td.date()
        return res


class ClassificationRule:
    """Rule for classifying transactions

    Classification is based on an expression and specifiers on how to
    classify transactions.
    """

    def __init__(self, rule: str, classifications: Dict) -> None:
        self._rule = Rule(rule)
        self._classifications = classifications

    def classify(self, transaction: Dict) -> Dict:
        try:
            if self._rule.matches(transaction):
                for k, v in self._classifications.items():
                    if k not in transaction or transaction[k] == "":
                        transaction[k] = v
        except EvaluationError:
            # Can occur if rules tries to match string, but is nan, etc.
            pass

        return transaction


class Rules:
    """A set of rules

    This is a programatic representation of the rules file.
    (Normally `rules.yaml`)
    """

    def __init__(self) -> None:
        self._import_rules = []
        self._classifiers = None

    def load(self, path: os.PathLike):
        """Loads a given rules file

        The rules file is expected to be a YAML file.

        Args:
            path (os.PathLike): _description_
        """
        yaml = YAML()
        try:
            with open(path) as file:
                p = yaml.load(file)
                if "import" not in p:
                    raise ValueError(f"Rules file '{path}' is missing field 'import'")
                self._import_rules = [ImportRule(**r) for r in p["import"]]
                if "classifiers" in p:
                    self._classifiers = [
                        ClassificationRule(**r) for r in p["classifiers"]
                    ]
                else:
                    self._classifiers = None
        except FileNotFoundError:
            raise FileNotFoundError(f"Could not find rules file: '{path}'")

    def load_dir(self, path: os.PathLike) -> List[Dict]:
        """Loads all files in a directory

        This will not recursively load the files; only files at the first
        level will be loaded.

        Loaded involves:

         - Selecting files that matches a given parser
         - Apply categorization rules to those files

        Args:
            path (os.PathLike): Directory to load

        Returns:
            List[Dict]: Returns parsed rows from files
        """

        # Locate tuples of rules and files to process
        parsers = []
        for file in os.listdir(path):
            for rule in self._import_rules:
                if rule.matches_file(Path(file)):
                    parsers.append((rule, os.path.join(path, file)))
                    break

        # Process the identified rules and files
        data = []
        for rule, file in parsers:
            d = rule.load(file)
            data.extend(d)

        if self._classifiers is not None:
            for d in data:
                for rule in self._classifiers:
                    d = rule.classify(d)
        return data

    @property
    def import_rules(self) -> List[ImportRule]:
        """List of import rules collected from rules file"""
        return self._import_rules
