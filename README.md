# Pynomics

Pynomics is a simple utility for managing your home finances.

Its primary purpose is to provide semi-automatic classification of transactions
and to analyze the data.

The usage flow is as follows:

 1. Setup a rules file.
 2. Download transactions from your bank.
 3. Run the `load` command on the transactions.
 4. Manually edit the Excel file to categorize transactions that are not
    covered by the rules.
 5. Run the `insights` command to analyze the transactions.

## The Rules File

The rules file defines two types of rules:

- Parsing rules that are used to interpret various banks format
- Classifiers that categorize transactions automatically

By default, the program looks for a `rules.yaml` file in the current directory.

### Parsing Rules

Parsing rules are defined in the `import` section.
Multiple rules can be defined to accomodate for different sources.
For each found file, the program will check if any parser have defined a
matcher that matches the filename.
The matcher is defined in the `match` key and uses regular expressions.

If the there are multiple parsers that matches, only the first matcher will be
applied.

>![NOTE] Column renaming
> Columns must be renamed so that they match the consistent column namns.
> This means `columns` should, if header in file does not match already,
> provide a mapping to the following names:
>
> - `account`
> - `amount`
> - `currency`
> - `transaction_date`
> - `payee`
> - `note`

An example of a parsing rule:

```yaml
import:
    -
        "account": "Test account" # Used to name where transactions occured (currently not used)
        "skip_rows":  2           # Number of rows to skip in file
        "match": "sample_file.*"  # Regex to match files with
        "date_format": "%Y-%m-%d" # Date format
        "columns":                # Column renamning
            "account": "Account"
            "amount": "Amount"      # Example: Nineth column is used to identify amount
            "currency": "Currency"
            "transaction_date": "Transaction date"
            "payee": "Reference"
            "note": "Description"
```

### Classifiers

Classifiers are used to automatically assign properties to transactions.
It uses a flexible format that allows adding custom columns if wanted.

Rules can evaluate columns and uses a natural syntax.
Under the hood it uses the
[`rule-engine`](https://github.com/zeroSteiner/rule-engine).
Details on the syntax is provided
[here](https://zerosteiner.github.io/rule-engine/syntax.html).

Available columns to use in rules:

- `account`
- `amount`
- `currency`
- `transaction_date`
- `payee`
- `note`

If a previous rule have already classified the transaction, it will not be
modified by subsequent classifiers.

An example of a classifier that will check the `payee` column for "REF C" and
if it matches assign a category of "housing" a "subcategory" of "heating" and
add an additional, custom, column of `tags` with value of "running_cost".

```yaml
classifiers:
    -
        "rule": "payee == \"REF C\""
        "classifications":
            "category": "housing"
            "subcategory": "heating"
            "tags": "running_costs"
```
